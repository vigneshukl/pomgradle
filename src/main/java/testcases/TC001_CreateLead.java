package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "Leads should be created successfully";
		authors = "Vignesh";
		category = "smoke";
		dataSheetName = "TC001";
		testNodes = "CreateLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String companyname,String firstname, String lastname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.ClickCRM()
		.ClickLeads()
		.ClickCreateLead()
		.EnterCompanyName(companyname)
		.EnterFirstName(firstname)
		.EnterLastName(lastname)
		.ClickCreateLeadButton()
		.VerifyFirstName(firstname);
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
