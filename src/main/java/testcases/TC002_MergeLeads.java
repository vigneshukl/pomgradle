package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_MergeLeads extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_MergeLeads";
		testDescription = "Merge should happen successfully";
		authors = "Vignesh";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "MergeLeads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String firstidvalue,String secondidvalue) throws InterruptedException {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.ClickCRM()
		.ClickLeads()
		.ClickMergeLeads()
		.ClickFromLeadIcon()
		.EnterLeadIdValue(firstidvalue)
		.ClickFindLeadsButton()
		.ClickFirstResultingLead()
		.ClickToLeadIcon()
		.EnterLeadIdValue1(secondidvalue)
		.ClickFindLeadsButton1()
		.ClickFirstResultingLead1()
		.ClickMergeButton()
		.ClickFindLeads()
		.EnterLeadIdToFind(firstidvalue)
		.ClickFindLeadsButtoninFindLeadspage()
		.VerifyOutput();
		
		
		
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
