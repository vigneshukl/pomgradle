package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.FindLeadsPage;
import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//input[@name='id']") WebElement enterleadidtofind;
	@FindBy(xpath="//button[text()='Find Leads']") WebElement clickfindleads;
	@FindBy(xpath="//div[text()='No records to display']") WebElement verifyoutput;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement clickfindleads1;
	
	public FindLeadsPage ClickFindLeads() {
		click(clickfindleads1);
		return this;
	}
	
	public FindLeadsPage EnterLeadIdToFind(String data) {	
		type(enterleadidtofind, data);
		return this;
	}
	
	public FindLeadsPage ClickFindLeadsButtoninFindLeadspage() throws InterruptedException {	
		click(clickfindleads);
		Thread.sleep(1000);
		return this;
	}
	
	public FindLeadsPage VerifyOutput() {		
		verifyExactText(verifyoutput, "No records to display");
		return this;
	}
	

	
	








}
