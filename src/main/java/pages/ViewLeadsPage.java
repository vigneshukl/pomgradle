package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.ViewLeadsPage;
import wdMethods.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{

	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="viewLead_firstName_sp")
	WebElement verifyfirstname;
	
	
	
	public FindLeadsPage VerifyFirstName(String data) {		
		verifyExactText(verifyfirstname, data);
		return new FindLeadsPage();
	}
	
	
	
	








}
