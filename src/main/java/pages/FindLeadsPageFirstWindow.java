package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.FindLeadsPageFirstWindow;
import wdMethods.ProjectMethods;

public class FindLeadsPageFirstWindow extends ProjectMethods{

	public FindLeadsPageFirstWindow() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//input[@type='text'])[1]") WebElement leadidvalue;
	@FindBy(xpath="(//button[@class='x-btn-text'])[1]") WebElement clickfindleadsbutton;
	@FindBy(className="linktext") WebElement clickfirstresultinglead;
	
	public FindLeadsPageFirstWindow EnterLeadIdValue(String data) {	
		switchToWindow(1);
		type(leadidvalue, data);
		return this;
	}
	
	public FindLeadsPageFirstWindow ClickFindLeadsButton() throws InterruptedException {	
		click(clickfindleadsbutton);
		Thread.sleep(1000);
		return this;
	}
	
	public MergeLeadsPage ClickFirstResultingLead() {
		click(clickfirstresultinglead);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	
	

	
	








}
