Feature: Login into LeafTaps

#Background:
#Given Open the web browser 
#And Maximize the browser
#And Set the Timeout
#And Launch the URL


Scenario Outline: CreateLead Test case
And Enter the username as <username> 
And Enter the password as <password>
And Click on the Login Button
And Click on CRMSFA
And Click on Leads
And Click CreateLead
And Enter CompanyName as <companyname>
And Enter FirstName as <firstname>
And Enter LastName as <lastname>
When Click on Create Lead
#Then Lead should be created successfully
#Then close browser

Examples:
|username|password|companyname|firstname|lastname|
|DemoSalesManager|crmsfa|IBM|Vignesh|K|
|DemoSalesManager|crmsfa|CTS|Sivaram|K|

Scenario Outline: Login Test case with Examples
And Enter the username as <username> 
And Enter the password as <password>
And Click on the Login Button
#Then close browser

Examples:
|username|password|
|DemoCSR|crmsfa|

Scenario: Login Test case without Examples
And Enter the username as DemoCSR 
And Enter the password as crmsfa
And Click on the Login Button
#Then close browser
