package Steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import wdMethods.SeMethods;
import cucumber.api.java.After;

public class Hooks extends SeMethods{
	
	@Before
	public void before(Scenario sc)
	{
		startResult();
		startTestModule(sc.getName(),sc.getId());
		test = startTestCase(sc.getName());
		test.assignCategory("Functional");
		test.assignAuthor("Vignesh");
		startApp("chrome", "http://leaftaps.com/opentaps");	
//		System.out.println(sc.getName());
//		System.out.println(sc.getId());
	}
	@After
	public void After(Scenario sc)
	{
		closeAllBrowsers();
		endResult();
//		System.out.println(sc.getStatus());
//		System.out.println(sc.isFailed());
	}

}
